# jmmc-exist-kubernetes

K8S config for the stateless jmmc's eXistDB instance(s).

* Check the backend you want to update:
  * kubectl describe ingress exist-blue-green-ingress
* Kustomize the right color
  * vi overlays/blue/kustomization.yml
* Apply it
  * kustomize build overlays/blue | kubectl apply -f -

You may have a look on https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-exist-docker/container_registry to find the right image tag

